package com.management.managementapp.task;

import com.management.managementapp.exceptions.BadRequestException;
import com.management.managementapp.student.Student;
import com.management.managementapp.student.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@AllArgsConstructor
@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final StudentRepository studentRepository;

    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    //TODO: create possibility to get task by id

    public void createTask(Task task) {
        Boolean taskExists = taskRepository.selectExistsByName(task.getTaskName());

        if (taskExists) {
            throw new BadRequestException("Task " + task.getTaskName() + " already exists");
        }
        taskRepository.save(task);
    }

    public void assignStudentToTask(Long taskId, Long studentId) throws Exception {
        if (!taskRepository.existsById(taskId)) {
            throw new Exception("Task doesnt exists");
        }

        if (!studentRepository.existsById(studentId)) {
            throw new Exception("Student doesnt exists");
        }

        if (!taskRepository.existsStudentInTask(studentId)) {
            Task task = taskRepository.getOne(taskId);
            Student student = studentRepository.getOne(studentId);

            task.setAssignedStudents(Collections.singleton(student));
            taskRepository.save(task);
        } else {
            throw new Exception("Student already assigned to task");
        }
    }
}
