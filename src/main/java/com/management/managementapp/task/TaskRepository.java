package com.management.managementapp.task;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    @Query("" +
            "SELECT CASE WHEN COUNT(task) > 0 THEN " +
            "TRUE ELSE FALSE END " +
            "FROM Task task " +
            "WHERE task.taskName = ?1"
    )
    Boolean selectExistsByName(String taskName);

    @Query("select case when count(task) > 0 then true else false end from Task task inner join task.assignedStudents s where s.studentId = :studentId")
    Boolean existsStudentInTask(Long studentId);
}
