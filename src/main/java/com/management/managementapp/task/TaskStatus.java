package com.management.managementapp.task;

public enum TaskStatus {
    OPEN,
    IN_PROGRESS,
    CLOSED
}
