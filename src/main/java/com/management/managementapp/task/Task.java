package com.management.managementapp.task;

import com.management.managementapp.student.Student;
import jdk.jfr.Timestamp;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tasks")
public class Task {
    @Id
    @SequenceGenerator(
            name = "task_sequence",
            sequenceName = "task_sequence",
            allocationSize = 1
    )
    private Long id;

    @NotBlank
    @NotNull
    private String taskName;

    @NotBlank
    @NotNull
    private String question;

    private String taskSolution;

    @Enumerated(EnumType.STRING)
    private TaskStatus taskStatus;

    @Timestamp
    private Long taskAssignedDate;

    @ManyToMany
    @JoinTable(
            name = "assigned_student",
            joinColumns = @JoinColumn(name = "task_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private Set<Student> assignedStudents = new HashSet<>();

}
