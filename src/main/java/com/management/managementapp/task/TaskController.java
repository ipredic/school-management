package com.management.managementapp.task;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/task")
@AllArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/all-tasks")
    public List<Task> getAllTasks() {
        return taskService.getAllTasks();
    }

    @PostMapping("/create")
    public void createTask(@Valid @RequestBody Task task) {
        taskService.createTask(task);
    }

    @PutMapping("/{taskId}/students/{studentId}")
    public void assignStudentsToTask(@PathVariable("taskId") Long taskId, @PathVariable("studentId") Long studentId) throws Exception {
        taskService.assignStudentToTask(taskId, studentId);
    }
}
