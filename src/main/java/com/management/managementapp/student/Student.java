package com.management.managementapp.student;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.management.managementapp.task.Task;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@ToString
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "student")
public class Student {
    @Id
    @SequenceGenerator(
            name = "student_sequence",
            sequenceName = "student_sequence",
            allocationSize = 1
    )
    @GeneratedValue(generator = "student_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "student_id")
    private Long studentId;

    @NotBlank
    private String name;

    @Email
    @Column(nullable = false, unique = true)
    private String email;

    @JsonIgnore
    @ManyToMany(mappedBy = "assignedStudents")
    private Set<Task> tasks = new HashSet<>();
}
