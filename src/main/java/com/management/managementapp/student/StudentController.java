package com.management.managementapp.student;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @RolesAllowed("admin")
    @GetMapping("student/all")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @RolesAllowed("admin")
    @PostMapping("student/add")
    public void addStudent(@Valid @RequestBody Student student) {
        studentService.addStudent(student);
    }

    @RolesAllowed("admin")
    @DeleteMapping("student/{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long studentId) {
        studentService.deleteStudent(studentId);
    }
}
