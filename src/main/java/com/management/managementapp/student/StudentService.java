package com.management.managementapp.student;

import com.management.managementapp.exceptions.BadRequestException;
import com.management.managementapp.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class StudentService {

    private final StudentRepository studentRepository;

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public void addStudent(Student student) {
        Boolean studentExists = studentRepository.selectExistsEmail(student.getEmail());

        if (studentExists) {
            throw new BadRequestException("Email " + student.getEmail() + " is taken");
        }
        studentRepository.save(student);
    }

    public void deleteStudent(Long studentId) {
        if(!studentRepository.existsById(studentId)) {
            throw new NotFoundException("Student does not exists");
        }
        studentRepository.deleteById(studentId);
    }
}
