package com.management.managementapp.integration;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

//TODO: import information about keycloak token for admin role
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-it.properties")
public class StudentIT {

//    @Autowired
//    private MockMvc mockMvc;
//
//    @Autowired
//    private ObjectMapper objectMapper;
//
//    @Autowired
//    private StudentRepository studentRepository;
//
//    @Test
//    void canAddNewStudent() throws Exception {
//        //given
//        Student student = new Student(1L, "max", "max@gmail.com", Gender.MALE);
//        //when
//        ResultActions resultActions = mockMvc.perform(post("/api/student/all").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(student)));
//        //then
//        resultActions.andExpect(status().isOk());
//        List<Student> students = studentRepository.findAll();
//        assertThat(students).usingElementComparatorIgnoringFields("id").contains(student);
//    }
}
