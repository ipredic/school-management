package com.management.managementapp.task;

import com.management.managementapp.exceptions.BadRequestException;
import com.management.managementapp.student.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import java.util.Date;


public class TaskServiceTest {
    @Mock
    private TaskRepository taskRepository;
    @Mock
    private StudentRepository studentRepository;
    private AutoCloseable autoCloseable;
    private TaskService underTest;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        underTest = new TaskService(taskRepository, studentRepository);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void canGetAllTasks() {
        //when
        underTest.getAllTasks();
        //then
        verify(taskRepository).findAll();
    }

    @Test
    void canCreateTask() {
        //given
        Date date = new Date();
        Task task = new Task(1L,
                "Math problem II",
                "1+1=?",
                null,
                TaskStatus.OPEN,
                date.getTime() / 1000L,
                null);
        //when
        underTest.createTask(task);
        //then
        ArgumentCaptor<Task> taskArgumentCaptor = ArgumentCaptor.forClass(Task.class);
        verify(taskRepository).save(taskArgumentCaptor.capture());
        Task capturedTask = taskArgumentCaptor.getValue();
        assertThat(capturedTask).isEqualTo(task);
    }

    @Test
    void willThrowWhenTaskExistsByName() {
        //given
        Date date = new Date();
        Task task = new Task(1L,
                "Math problem II",
                "1+1=?",
                null,
                TaskStatus.OPEN,
                date.getTime() / 1000L,
                null);
        given(taskRepository.selectExistsByName(task.getTaskName())).willReturn(true);
        //when
        //then
        assertThatThrownBy(() -> underTest.createTask(task)).isInstanceOf(BadRequestException.class).hasMessageContaining("Task " + task.getTaskName() + " already exists");
    }

    @Test
    void willThrowWhenTaskIdNotFound() {
        //given
        long taskId = 2;
        long studentId = 1;
        given(taskRepository.existsById(taskId)).willReturn(false);
        //when
        //then
        assertThatThrownBy(() -> underTest.assignStudentToTask(taskId, studentId)).isInstanceOf(Exception.class).hasMessageContaining("Task doesnt exists");
        verify(taskRepository).existsById(taskId);
    }

    @Test
    void willThrowWhenStudentIdNotFound() {
        //given
        long taskId = 2;
        long studentId = 1;
        given(taskRepository.existsById(taskId)).willReturn(true);
        given(studentRepository.existsById(studentId)).willReturn(false);
        //when
        //then
        assertThatThrownBy(() -> underTest.assignStudentToTask(taskId, studentId)).isInstanceOf(Exception.class).hasMessageContaining("Student doesnt exists");
        verify(studentRepository).existsById(studentId);
    }

    @Test
    void willThrowWhenStudentsExistsInTask() {
        //given
        long taskId = 2;
        long studentId = 1;
        given(taskRepository.existsById(taskId)).willReturn(true);
        given(studentRepository.existsById(studentId)).willReturn(true);
        given(taskRepository.existsStudentInTask(studentId)).willReturn(true);
        //when
        //then
        assertThatThrownBy(() -> underTest.assignStudentToTask(taskId, studentId)).isInstanceOf(Exception.class).hasMessageContaining("Student already assigned to task");
    }

}
