package com.management.managementapp.task;

import com.management.managementapp.student.Student;
import com.management.managementapp.student.StudentRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Collections;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
public class TaskRepositoryTest {
    @Autowired
    private TaskRepository underTest;
    @Autowired
    private StudentRepository underTestStudent;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldCheckIfTaskExistsByName() {
        //given
        Task task = new Task(1L,
                "Resolve Math problem",
                "1+1=?",
                null,
                TaskStatus.OPEN,
                1642970635L,
                null
        );
        underTest.save(task);
        //when
        boolean exists = underTest.selectExistsByName(task.getTaskName());
        //then
        assertThat(exists).isTrue();
    }

    @Test
    void itShouldCheckIfTaskDoesNotExistsByName() {
        //given
        String taskName = "Resolve Math problem";
        //when
        boolean exists = underTest.selectExistsByName(taskName);
        //then
        assertThat(exists).isFalse();
    }

    @Test
    void itShouldCheckIfStudentIsNotAssignedToTask() {
        //given
        Student student = new Student(1L, "Ivan", "ivanp@gmail.com", null);
        //when
        boolean exists = underTest.existsStudentInTask(student.getStudentId());
        //then
        assertThat(exists).isFalse();
    }

    @Test
    void itShouldCheckIfStudentIsAssignedToTask() {
        //given
        Student student = new Student(1L, "Ivan", "ivanp@gmail.com", null);
        Task task = new Task(1L,
                "Resolve Math problem",
                "1+1=?",
                null,
                TaskStatus.OPEN,
                1642970635L,
                Collections.singleton(student)
        );
        underTestStudent.save(student);
        underTest.save(task);
        //when
        boolean exists = underTest.existsStudentInTask(student.getStudentId());
        //then
        assertThat(exists).isTrue();
    }
}
