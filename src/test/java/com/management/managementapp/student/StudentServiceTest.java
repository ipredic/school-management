package com.management.managementapp.student;

import com.management.managementapp.exceptions.BadRequestException;
import com.management.managementapp.exceptions.NotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;
    private AutoCloseable autoCloseable;
    private StudentService underTest;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        underTest = new StudentService(studentRepository);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void canGetAllStudents() {
        //when
        underTest.getAllStudents();
        //then
        verify(studentRepository).findAll();
    }

    @Test
    void canAddStudent() {
        //given
        Student student = new Student(1L, "Max", "mac@gmail.com", null);
        //when
        underTest.addStudent(student);
        //then
        ArgumentCaptor<Student> studentArgumentCaptor = ArgumentCaptor.forClass(Student.class);
        verify(studentRepository).save(studentArgumentCaptor.capture());
        Student capturedStudent = studentArgumentCaptor.getValue();

        assertThat(capturedStudent).isEqualTo(student);
    }

    @Test
    void willThrowIfEmailIsTaken() {
        //given
        Student student = new Student(1L, "Max", "mac@gmail.com", null);
        given(studentRepository.selectExistsEmail(student.getEmail())).willReturn(true);
        //when
        //then
        assertThatThrownBy(() -> underTest.addStudent(student)).isInstanceOf(BadRequestException.class).hasMessageContaining("Email " + student.getEmail() + " is taken");
        verify(studentRepository, never()).save(any());
    }

    @Test
    void canDeleteStudent() {
        //given
        long studentId = 10;
        given(studentRepository.existsById(studentId)).willReturn(true);
        //when
        underTest.deleteStudent(studentId);
        //then
        verify(studentRepository).deleteById(studentId);
    }

    @Test
    void willThrowWhenDeleteStudentNotFound() {
        //given
        long studentId = 10;
        given(studentRepository.existsById(studentId)).willReturn(false);
        //when
        //then
        assertThatThrownBy(() -> underTest.deleteStudent(studentId)).isInstanceOf(NotFoundException.class).hasMessageContaining("Student does not exists");
        verify(studentRepository, never()).deleteById(any());
    }
}