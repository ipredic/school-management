package com.management.managementapp.student;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class StudentRepositoryTest {
    @Autowired
    private StudentRepository underTest;

    @AfterEach
    void tearDown() {
        underTest.deleteAll();
    }

    @Test
    void itShouldCheckIfStudentExistsByEmail() {
        //given
        String name = "Ivan";
        String email = "ip@gmail.com";
        Student student = new Student(1L, name, email, null);
        underTest.save(student);
        //when
        boolean exists = underTest.selectExistsEmail(email);
        //then
        assertThat(exists).isTrue();
    }

    @Test
    void itShouldCheckIfStudentDoesNotExistsByEmail() {
        //given
        String email = "ip@gmail.com";
        //when
        boolean exists = underTest.selectExistsEmail(email);
        //then
        assertThat(exists).isFalse();
    }
}