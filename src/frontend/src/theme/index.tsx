import React from "react";
import { useMemo } from "react";
import {
  Breakpoints,
  ComponentsProps,
  CssBaseline,
  Palette,
  ThemeWithProps,
  Transitions,
  Typography,
} from "@mui/material";
import {
  ThemeProvider,
  createTheme,
  StyledEngineProvider,
} from "@mui/material/styles";
import shape from "./shape";
import palette from "./palette";
import typography from "./typography";
import componentsOverride from "./overrides";
import shadows, { customShadows } from "./shadows";
import { ZIndex } from "@mui/material/styles/zIndex";
import { Shadows } from "@mui/material/styles/shadows";
import { Mixins } from "@mui/material/styles/createMixins";

interface ThemeConfigProps {
  children: React.ReactNode;
}

export interface ThemeProps {
  breakpoints?: Breakpoints;
  mixins?: Mixins;
  palette?: Palette;
  props?: ComponentsProps;
  shadows?: Shadows;
  transitions?: Transitions;
  typography?: typeof Typography;
  zIndex?: ZIndex;
}

const ThemeConfig = ({ children }: ThemeConfigProps): JSX.Element => {
  const themeOptions = useMemo(
    () => ({
      palette,
      shape,
      typography,
      shadows,
      customShadows,
    }),
    []
  );

  const theme = createTheme(themeOptions as ThemeWithProps);
  theme.components = componentsOverride(theme);

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default ThemeConfig;
