import { useState } from "react";
import * as Yup from "yup";
import { useFormik, Form, FormikProvider } from "formik";

import { Box, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";

const UserInvitationForm = () => {
  const RegisterSchema = Yup.object().shape({
    userName: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("First name required"),
    email: Yup.string()
      .email("Email must be a valid email address")
      .required("Email is required"),
  });

  const formik = useFormik({
    initialValues: {
      userName: "",
      email: "",
      // realmRoles: "",
    },
    validationSchema: RegisterSchema,
    onSubmit: () => {
      console.log("submit");
    },
  });

  const { errors, touched, handleSubmit, isSubmitting, getFieldProps } = formik;

  return (
    <FormikProvider value={formik}>
      <Form autoComplete="off" noValidate onSubmit={handleSubmit}>
        <Box
          sx={{
            "& > :not(style)": {
              m: 1,
              width: "35ch",
            },
            "&>:last-child": { padding: "27px" },
          }}
        >
          <TextField
            fullWidth
            label="Username"
            {...getFieldProps("userName")}
            error={Boolean(touched.userName && errors.userName)}
            helperText={touched.userName && errors.userName}
            size={"medium"}
          />
          <TextField
            fullWidth
            type="email"
            label="Email address"
            {...getFieldProps("email")}
            error={Boolean(touched.email && errors.email)}
            helperText={touched.email && errors.email}
          />
          <LoadingButton
            fullWidth
            type="submit"
            size="large"
            variant="contained"
            loading={isSubmitting}
          >
            Invite User
          </LoadingButton>
        </Box>
      </Form>
    </FormikProvider>
  );
};

export default UserInvitationForm;
