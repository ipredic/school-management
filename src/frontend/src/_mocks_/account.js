const account = {
  displayName: "Ivan Predic",
  email: "demo@minimals.cc",
  photoURL: "/static/mock-images/avatars/avatar_default.jpg",
};

export default account;
