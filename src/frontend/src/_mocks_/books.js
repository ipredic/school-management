import faker from "faker";
import { mockImgProduct } from "../utils/mockImages";

const BOOK_NAME = ["Math", "C++"];

const books = [...Array(2)].map((_, index) => {
  const setIndex = index + 1;

  return {
    id: faker.datatype.uuid(),
    cover: mockImgProduct(setIndex),
    name: BOOK_NAME[index],
  };
});

export default books;
