import { Grid } from "@mui/material";
import BookCard from "./BookCard";

interface BooksListProps {
  books: Array<{ id: number; cover: string; name: string }>;
}

const BooksList = ({ books, ...other }: BooksListProps) => {
  return (
    <Grid container spacing={3} {...other}>
      {books.map((book) => (
        <Grid key={book.id} item xs={12} sm={6} md={3}>
          <BookCard book={book} />
        </Grid>
      ))}
    </Grid>
  );
};

export default BooksList;
