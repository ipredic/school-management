import React from "react";
import { Box } from "@mui/material";

interface LogoPropTypes {
  sx?: object;
}

const Logo = ({ sx }: LogoPropTypes) => {
  return (
    <Box
      component="img"
      src="/static/logo.svg"
      sx={{ width: 40, height: 40, ...sx }}
    />
  );
};

export default Logo;
