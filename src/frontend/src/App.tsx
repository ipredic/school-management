import React from "react";
import { ReactKeycloakProvider } from "@react-keycloak/web";

import ThemeConfig from "./theme";
import GlobalStyles from "./theme/globalStyles";
import ScrollToTop from "./components/ScrollToTop";
import Router from "./routes";
import UserService from "./security/UserService";

export const App = () => {
  const { _kc, initOptions } = UserService;

  return (
    <ReactKeycloakProvider
      authClient={_kc}
      initOptions={initOptions}
      LoadingComponent={<h1>Loading...</h1>}
    >
      <ThemeConfig>
        <ScrollToTop />
        <GlobalStyles />
        <Router />
      </ThemeConfig>
    </ReactKeycloakProvider>
  );
};
