import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from "axios";
import UserService from "../security/UserService";

export const customInstance = () => {
  const token = UserService.getToken();

  const axiosInstance: AxiosInstance = axios.create({
    baseURL: "http://localhost:8081/api/",
  });

  const onRequest = (config: AxiosRequestConfig): AxiosRequestConfig => {
    if (UserService.isLoggedIn()) {
      const cb = () => {
        config.headers.Authorization = `Bearer ${token}`;
        return Promise.resolve(config);
      };
      return UserService.updateToken(cb);
    }
    return config;
  };

  const onRequestError = (error: AxiosError): Promise<AxiosError> => {
    console.error(`[request error] [${JSON.stringify(error)}]`);
    return Promise.reject(error);
  };

  const onResponse = (response: AxiosResponse): AxiosResponse => {
    console.info(`[response] [${JSON.stringify(response)}]`);
    return response;
  };

  const onResponseError = (error: AxiosError): Promise<AxiosError> => {
    console.error(`[response error] [${JSON.stringify(error)}]`);
    return Promise.reject(error);
  };

  axiosInstance.interceptors.request.use(onRequest, onRequestError);
  axiosInstance.interceptors.response.use(onResponse, onResponseError);

  return axiosInstance;
};
