import { Icon, IconifyIcon } from "@iconify/react";
import pieChart2Fill from "@iconify/icons-eva/pie-chart-2-fill";
import peopleFill from "@iconify/icons-eva/people-fill";
import shoppingBagFill from "@iconify/icons-eva/shopping-bag-fill";

const getIcon = (name: IconifyIcon) => (
  <Icon icon={name} width={22} height={22} />
);

interface SidebarConfigProps {
  title: string;
  path: string;
  icon: JSX.Element;
}

const sidebarConfig: SidebarConfigProps[] = [
  {
    title: "dashboard",
    path: "/dashboard/app",
    icon: getIcon(pieChart2Fill),
  },
  {
    title: "student",
    path: "/dashboard/student",
    icon: getIcon(peopleFill),
  },
  {
    title: "books",
    path: "/dashboard/books",
    icon: getIcon(shoppingBagFill),
  },
];

export default sidebarConfig;
