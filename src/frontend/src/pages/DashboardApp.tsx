import { Box, Grid, Container, Typography } from "@mui/material";
import appleFilled from "@iconify/icons-ant-design/apple-filled";
import Page from "../components/Page";
import { AppTasks, Widget } from "../components/_dashboard/app";
import UserService from "../security/UserService";

const DashboardApp = (): JSX.Element => {
  return (
    <Page title="Dashboard">
      {UserService.hasRole(["ROLE-ADMIN"]) ? (
        <Container maxWidth="xl">
          <Box sx={{ pb: 5 }}>
            <Typography variant="h4">Hi, Welcome back Admin</Typography>
          </Box>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6} md={3}>
              <Widget title={"Users"} widgetIcon={appleFilled} amount={100} />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Widget title={"Users"} widgetIcon={appleFilled} amount={100} />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Widget title={"Users"} widgetIcon={appleFilled} amount={100} />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <Widget title={"Users"} widgetIcon={appleFilled} amount={100} />
            </Grid>
            <Grid item xs={12} md={6} lg={8}>
              <AppTasks />
            </Grid>
          </Grid>
        </Container>
      ) : (
        // Student Dashboard logic here
        <Box sx={{ pb: 5 }}>
          <Typography variant="h4">Hi, Welcome Student</Typography>
        </Box>
      )}
    </Page>
  );
};

export default DashboardApp;
