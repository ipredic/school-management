import React, { useState } from "react";
import { Icon } from "@iconify/react";
import plusFill from "@iconify/icons-eva/plus-fill";
import closeFill from "@iconify/icons-eva/close-fill";
import { Link as RouterLink } from "react-router-dom";
import { Stack, Button, Container, Typography } from "@mui/material";
import Page from "../components/Page";
import UserInvitationForm from "../containers/UserInvitationForm";

const Student = (): JSX.Element => {
  const [isInvitationFormVisible, setInvitationFormVisible] = useState(false);

  const handleInvitationFormVisibility = () => {
    setInvitationFormVisible(!isInvitationFormVisible);
  };

  return (
    <Page title="Student">
      <Container>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          mb={5}
        >
          <Typography variant="h4" gutterBottom>
            Students
          </Typography>
          <Button
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={
              !isInvitationFormVisible ? (
                <Icon icon={plusFill} />
              ) : (
                <Icon icon={closeFill} />
              )
            }
            onClick={handleInvitationFormVisibility}
          >
            {!isInvitationFormVisible ? "New Student" : "Close Invitation Form"}
          </Button>
        </Stack>
        {isInvitationFormVisible && <UserInvitationForm />}
      </Container>
    </Page>
  );
};

export default Student;
