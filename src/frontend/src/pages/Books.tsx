import { useFormik } from "formik";
import { useState } from "react";
import { Container, Stack, Typography } from "@mui/material";
import Page from "../components/Page";
import { BookSort, BooksList } from "../components/_dashboard/books";
import BOOKS from "../_mocks_/books";

const Books = () => {
  const [openFilter, setOpenFilter] = useState(false);

  const formik = useFormik({
    initialValues: {
      category: "",
    },
    onSubmit: () => {
      setOpenFilter(false);
    },
  });

  const { resetForm, handleSubmit } = formik;

  return (
    <Page title="Books">
      <Container>
        <Typography variant="h4" sx={{ mb: 5 }}>
          Products
        </Typography>

        <Stack
          direction="row"
          flexWrap="wrap-reverse"
          alignItems="center"
          justifyContent="flex-end"
          sx={{ mb: 5 }}
        >
          <Stack direction="row" spacing={1} flexShrink={0} sx={{ my: 1 }}>
            <BookSort />
          </Stack>
        </Stack>

        <BooksList books={BOOKS} />
      </Container>
    </Page>
  );
};

export default Books;
